# -*- coding: utf-8 -*-

class OldPython(object):
    def __init__(self, age):
        if age < 50:
            raise ValueError("{} isn't old".format(age))
        self.age = age

    def hiss(self):
        if self.age < 60:
            return "sss ssss"
        elif self.age < 70:
            return "SSss SSss"
        else:
            return "sss... *cough* *cough*"