# https://stackoverflow.com/questions/42932655/does-sys-settrace-work-properly-in-python-3-5-but-not-in-python-3-6

import atexit
import collections
import pprint
import sys


#def main():
    #i = create_instance()
    #print(len(i.data), flush=True)

def doSomething():
    print("Hello World")

def main():
    i = create_instance()
    print(len(i.data))
    doSomething()
    print(len(i.data))

def create_instance():
    instance = Tracer()
    return instance


class Tracer:

    def __init__(self):
        self.data = []
        sys.settrace(self.trace)
        atexit.register(pprint.pprint, self.data)
        atexit.register(sys.settrace, None)

    def trace(self, frame, event, arg):
        print('Tracing ...', flush=True)
        self.data.append(TraceRecord(frame, event, arg))
        return self.trace


TraceRecord = collections.namedtuple('TraceRecord', 'frame, event, arg')


if __name__ == '__main__':
    main()