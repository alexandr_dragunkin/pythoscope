Предположим, вы работаете над каким то старым проектом Python. Он некрасивый и непредсказуемый, но у вас нет выбора, кроме как сохранить его в живых. К счастью вы слышали об этом "новом" инструменте под названием *Pythoscope*, который может помочь вам вылечишь старика.

Начинаем с перехода в каталог проекта:

	$ cd wild_pythons/

и инициализации внутренней структуры *Pythoscope*:

	$ pythoscope --init

Эта команда создает подкаталог **.pythoscope/**, который будет содержать всю информация, связанную с *Pythoscope*. 

![](\img\py_init.png)


Допусим, вы смотрите на эту бедную "змеюку":

> **Примечание:** Пример для версии 2.x

	# old_python.py
	class OldPython(object):
	    def __init__(self, age):
	        pass # more code...
	
	    def hiss(self):
	        pass # even more code...

и решаете, что это безобразие требует немедленного внимания. Итак, вы запускаете Pythoscope для этого старого файла:

	$ pythoscope old_python.py

![](\img\py_first_file_start.png)

и теперь, смотрите тестовый модуль, сгенерированный в каталоге **tests/** :
	
	
	# tests/test_old_python.py
	import unittest
	
	class TestOldPython(unittest.TestCase):
	    def test__init__(self):
	        # old_python = OldPython(age)
	        assert False # TODO: implement your test here (реализуйте свой тест здесь)
	
	    def test_hiss(self):
	        # old_python = OldPython(age)
	        # self.assertEqual(expected, old_python.hiss())
	        assert False # TODO: implement your test here (реализуйте свой тест здесь)
	
	if __name__ == '__main__':
    unittest.main()

![](\img\run_first_test.png)

Это отправная точка для того чтобы вам начать заниматься "тестовой борьбой", но *Pythoscope* обладает гораздо большим арсеналом приемчиков, которые могу вам помочь. Все, что вам нужно сделать, это дать ему больше информации о вашем проекте.

Поскольку Python является очень динамичным языком, неудивительно, что большая часть информации о приложении может быть собрана только во время выполнения. Но устаревшие приложения могут быть сложными и опасными, поэтому *Pythoscope* не будет запускать код вообще, если вы явно не сообщите ему об этом. Можно указать, какой код безопасен для выполнения, создав так называемые, точки входа.

Точка входа - это простой модуль Python, который выполняет некоторые части вашего кода.
Вы должны хранить каждую точку входа в отдельном файле в каталоге **.pythoscope/points-of-entry/**. Давайте посмотрим ближе на нашего старого друга:

> **Примечание:** Пример для версии 2.x
	
	# old_python.py
	class OldPython(object):
	    def __init__(self, age):
	        if age < 50:
	            raise ValueError("%d isn't old" % age)
	        self.age = age
	
	    def hiss(self):
	        if self.age < 60:
	            return "sss sss"
	        elif self.age < 70:
	            return "SSss SSss"
	        else:
	            return "sss... *cough* *cough*"

> **Примечание:** Все что ниже. По состоянию на 2018.03.22 для Python 3.x НЕ РАБОТАЕТ.
	
	# old_python.py
	class OldPython(object):
	    def __init__(self, age):
	        if age < 50:
	            raise ValueError("{} isn't old".format(age))
	        self.age = age
	
	    def hiss(self):
	        if self.age < 60:
	            return "sss sss"
	        elif self.age < 70:
	            return "SSss SSss"
	        else:
	            return "sss... *cough* *cough*"



Основываясь на этом определении, мы получаем следующую точку входа:
	
	# .pythoscope/points-of-entry/123_years_old_python.py
	from old_python import OldPython
	OldPython(123).hiss()

После этого мы можем попытаться создать новые тестовые случаи. Просто снова вызовите *pythoscope* для `old_python.py`:

	$ pythoscope old_python.py

![](\img\py_first_entry_point_file_start.png)

Pythoscope выполнит нашу новую точку входа и соберет, как можно больше динамической
информации, насколько это возможно. Если вы посмотрите на свой тестовый модуль, то заметите
добавлен новый тестовый случай:
	
	# tests/test_old_python.py
	import unittest
	from old_python import OldPython
	
	class TestOldPython(unittest.TestCase):
	    def test__init__(self):
	        # old_python = OldPython(age)
	        assert False # TODO: implement your test here
	
	    def test_hiss(self):
	        # old_python = OldPython(age)
	        # self.assertEqual(expected, old_python.hiss())
	        assert False # TODO: implement your test here
	
	    def test_hiss_returns_sss_cough_cough_after_creation_with_123(self):
	        old_python = OldPython(123)
	        self.assertEqual('sss... *cough* *cough*', old_python.hiss())
	
	if __name__ == '__main__':
	    unittest.main()

*Pythoscope* корректно перехватил создание объекта **OldPython** и вызвал его метод **hiss()**. Поздравляем, вы особо не трудились, но у вас уже есть первый тестовый пример! Но *Pythoscope* может дать гораздо больше, чем просто отдельные примеры тестов. Все зависит от точек входа, которые вы определите. Чем выше уровень, тем больше информации Pythoscope сможете собрать, что напрямую влияет на количество генерируемых тестов.

Так что давайте попробуем написать еще одну точку входа. Но сначала посмотрите на другой модуль, который у нас есть в нашем проекте:
	
	# old_nest.py
	from old_python import OldPython
	
	class OldNest(object):
	    def __init__(self, ages):
	        self.pythons = []
	        for age in ages:
	            try:
	                self.pythons.append(OldPython(age))
	            except ValueError:
	                pass # Ignore the youngsters.
	    def put_hand(self):
	        return '\n'.join([python.hiss() for python in self.pythons])

Этот модуль кажется немного повыше уровнем, чем **old_python.py**. Тем не менее, писать точку входа для него также просто::
	
	# .pythoscope/points-of-entry/old_nest_with_four_pythons.py
	from old_nest import OldNest
	OldNest([45, 55, 65, 75]).put_hand()

Не стесняйтесь и запустите *Pythoscope* сразу. Обратите внимание, что вы можете предоставить множество модулей в качестве аргументов - все они будут обрабатываться сразу:

	$ pythoscope old_python.py old_nest.py

Эта новая точка входа позволила не только создать тестовый пример для **OldNest**:
	
	# tests/test_old_nest.py
	import unittest
	from old_nest import OldNest
	
	class TestOldNest(unittest.TestCase):
	    def test_put_hand_returns_sss_sss_SSss_SSss_sss_cough_cough_after_creation_with_list(self):
	        old_nest = OldNest([45, 55, 65, 75])
	        self.assertEqual('sss sss\nSSss SSss\nsss... *cough* *cough*', old_nest.put_hand())
	
	if __name__ == '__main__':
	    unittest.main()

но также добавлены 4 новых тестовых случая для **OldPython**:
	
	def test_creation_with_45_raises_value_error(self):
	    self.assertRaises(ValueError, lambda: OldPython(45))
	
	def test_hiss_returns_SSss_SSss_after_creation_with_65(self):
	    old_python = OldPython(65)
	    self.assertEqual('SSss SSss', old_python.hiss())
	
	def test_hiss_returns_sss_cough_cough_after_creation_with_75(self):
	    old_python = OldPython(75)
	    self.assertEqual('sss... *cough* *cough*', old_python.hiss())
	
	def test_hiss_returns_sss_sss_after_creation_with_55(self):
	    old_python = OldPython(55)
	    self.assertEqual('sss sss', old_python.hiss())

Вы получили все это за две дополнительные строки кода. Еще лучше то, что вы можете безопасно модифицировать и расширять тестовые сценарии, созданные *Pythoscope*. После того, как вы напишете еще одну точку входа или добавите новое поведение в свою систему вы можете снова запустить *Pythoscope*, и он только добавит новые тестовые примеры к существующим тестовым модулям, сохраняя любые изменения, которые вы могли бы внести в них.

Это всё! По этому основному учебнику. Если у вас есть какие-либо вопросы, не стесняйтесь задавать их на [pythoscope google group](http://groups.google.com/group/pythoscope).
